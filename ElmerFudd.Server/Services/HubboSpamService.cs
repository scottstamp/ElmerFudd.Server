﻿using System;
using ElmerFudd.Server.Habbo;
using ElmerFudd.Server.Network.Protocol;
using System.Threading.Tasks;
using System.Threading;

namespace ElmerFudd.Server.Services
{
    public class HubboSpamService : ElmerService
    {
        private Motel motel;

        public HubboSpamService(Motel _motel)
        {
            motel = _motel;

            Commands.Add(motel.InHeaders.PerkAllowances, PerkAllowances);
            Commands.Add(motel.InHeaders.RoomModelInfo, RoomModelInfo);
            Commands.Add(motel.InHeaders.UserIsMuted, UserIsMuted);
        }

        private void SendNavSequence(HSession session, int roomId)
        {
            session.Remote.SendPacketAsync(motel.OutHeaders.RoomNavigate, roomId, "", -1).Wait();
            Thread.Sleep(250);
            session.Remote.SendPacketAsync(motel.OutHeaders.RoomNavSequence1).Wait();
            Thread.Sleep(50);
            session.Remote.SendPacketAsync(motel.OutHeaders.RoomNavSequence2).Wait();
            Thread.Sleep(50);
            session.Remote.SendPacketAsync(motel.OutHeaders.RoomNavSequence3).Wait();
            Thread.Sleep(100);
        }

        private void UserIsMuted(HSession session, HMessage message)
        {
            if (!session.IsMuted)
            {
                session.IsMuted = true;
                int seconds = message.ReadInteger();
                Program.Log($"[{session.ControllerId}] Muted for {seconds} seconds. Raiding paused.");

                Task.Factory.StartNew(() =>
                {
                    Thread.Sleep(seconds * 1000);
                    session.IsMuted = false;
                    Program.Log($"[{session.ControllerId}] Mute period over. Raiding resumed.");
                }, TaskCreationOptions.LongRunning);
            }
        }

        private void RoomModelInfo(HSession session, HMessage message)
        {
            message.ReadString();
            Program.Log($"[{session.ControllerId}] Entered room ID: {message.ReadInteger()}");
        }

        private void PerkAllowances(HSession session, HMessage message)
        {
			
            if (session.ControllerId == "scout") return;

            Task.Factory.StartNew(() =>
            {
                Thread.Sleep(500);
                SendNavSequence(session, 74372246);
                Thread.Sleep(200);
                session.Remote.SendPacketAsync(3607, "M", motel.Figures[motel.Randomizer.Next(motel.Figures.Length)]);
                Thread.Sleep(500);
                session.Remote.SendPacketAsync(272, 428513).Wait();
                Thread.Sleep(500);
                session.Remote.SendPacketAsync(272, 447373).Wait();
                Thread.Sleep(500);
				
				int randomNumber = 0;

                while (true)
                {
					try
					{
						while (motel.Phrases.Length < 2)
						{
							SendNavSequence(session, 0);
							Thread.Sleep(1000);
						}

						//if (!session.IsMuted)
						{
							if(motel.ChangeLooksOnEnter)
							{
								ChangeLooks(session, "M", motel.Figures[motel.Randomizer.Next(motel.Figures.Length)]);
							}
							
							RespectUser(session, 39964443);
							
							Random random = new Random(Guid.NewGuid().GetHashCode());
							
							if(motel.RaidRoomId == -1)
							{
								if(motel.RoomQueue.Count > 0)
								{
									var room = motel.RoomQueue.Dequeue();
									SendNavSequence(session, room.FlatId);
									
									if(motel.RunToRandomTile)
									{
										MoveTo(session, random.Next(motel.RunToRandomTileMin, motel.RunToRandomTileMax), random.Next(motel.RunToRandomTileMin, motel.RunToRandomTileMax));
									}
									
									if(motel.ShouldShout && motel.Phrases.Length >= 2)
									{
										randomNumber++;
										Shout(session, motel.Phrases[motel.Randomizer.Next(motel.Phrases.Length)] + " " + randomNumber, random.Next(3, 8));
										
										randomNumber++;
										Shout(session, motel.Phrases[motel.Randomizer.Next(motel.Phrases.Length)] + " " + randomNumber, random.Next(3, 8));
									}
									
									if (session.IsMuted)
										motel.RoomQueue.Enqueue(room);
								}
								else
								{
									SendNavSequence(session, 0);
									Thread.Sleep(1000);
								}
							}
							else
							{
								if(motel.RunToRandomTile)
								{
									MoveTo(session, random.Next(motel.RunToRandomTileMin, motel.RunToRandomTileMax), random.Next(motel.RunToRandomTileMin, motel.RunToRandomTileMax));
								}
								
								if(motel.ShouldLeave)
								{
									SendNavSequence(session, motel.RaidRoomId);
								}
								
								if(motel.ShouldShout && motel.Phrases.Length >= 2)
								{
									randomNumber++;
									Shout(session, motel.Phrases[motel.Randomizer.Next(motel.Phrases.Length)] + " " + randomNumber, random.Next(3, 8));
									
									randomNumber++;
									Shout(session, motel.Phrases[motel.Randomizer.Next(motel.Phrases.Length)] + " " + randomNumber, random.Next(3, 8));
								}
								
							}
						}
						/*else
						{
							if(motel.ShouldLeave)
							{
								SendNavSequence(session, 0);
								Thread.Sleep(1000);
							}
						}*/
					}
					catch
					{
						
					}
                }
            }, TaskCreationOptions.LongRunning);
        }

        private void RespectUser(HSession session, int id)
        {
            session.Remote.SendPacketAsync(motel.OutHeaders.RespectUser, id);
            Thread.Sleep(750);
            session.Remote.SendPacketAsync(motel.OutHeaders.RespectUser, id);
            Thread.Sleep(750);
            session.Remote.SendPacketAsync(motel.OutHeaders.RespectUser, id);
            Thread.Sleep(750);
        }

        private void Shout(HSession session, string text, int color)
        {
			try
			{
				//Program.Log($"[{session.ControllerId}] Shouted: " + text);
				session.Remote.SendPacketAsync(motel.OutHeaders.Shout, text, color).Wait();
				Thread.Sleep(750);
			}
			catch { }
        }
		
		private void MoveTo(HSession session, int x, int y)
        {
			try
			{
				//Program.Log($"[{session.ControllerId}] Shouted: " + text);
				session.Remote.SendPacketAsync(motel.OutHeaders.Walk, x, y).Wait();
				Thread.Sleep(50);
			}
			catch { }
        }
		
		private void ChangeLooks(HSession session, string gender, string looks)
        {
			try
			{
				//Program.Log($"[{session.ControllerId}] Shouted: " + text);
				session.Remote.SendPacketAsync((ushort)3607, gender, looks).Wait();
				Thread.Sleep(50);
			}
			catch { }
        }
    }
}