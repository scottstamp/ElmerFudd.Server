﻿using System;
using System.Collections.Generic;

using ElmerFudd.Server.Habbo;
using ElmerFudd.Server.Network.Protocol;

namespace ElmerFudd.Server.Services
{
    public abstract class ElmerService
    {
        protected Dictionary<ushort, Action<HSession, HMessage>> Commands { get; }

        public ElmerService()
        {
            Commands = new Dictionary<ushort, Action<HSession, HMessage>>();
        }

        public bool RunCommand(HSession session, HMessage packet)
        {
            if (Commands.TryGetValue(packet.Header, out Action<HSession, HMessage> command))
            {
                command(session, packet);
            }
            return (command != null);
        }
    }
}