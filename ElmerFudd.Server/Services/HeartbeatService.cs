﻿using ElmerFudd.Server.Habbo;
using ElmerFudd.Server.Network.Protocol;

namespace ElmerFudd.Server.Services
{
    public class HeartbeatService : ElmerService
    {
        private Motel motel;

        public HeartbeatService(Motel _motel)
        {
            motel = _motel;

            Commands.Add(motel.InHeaders.Ping, Ping);
        }

        private void Ping(HSession session, HMessage message)
        {
            session.Remote.SendPacketAsync(motel.OutHeaders.Pong).Wait();
        }
    }
}
