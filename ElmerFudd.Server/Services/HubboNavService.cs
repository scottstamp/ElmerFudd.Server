﻿using ElmerFudd.Server.Habbo;
using ElmerFudd.Server.Network.Protocol;
using System.Collections.Generic;
using ElmerFudd.Server.Services.Objects;
using System.Threading.Tasks;
using System.Threading;

namespace ElmerFudd.Server.Services
{
    public class HubboNavService : ElmerService
    {
        private Motel motel;

        public HubboNavService(Motel _motel)
        {
            motel = _motel;

            Commands.Add(motel.InHeaders.PerkAllowances, PerkAllowances);
            Commands.Add(motel.InHeaders.NavigatorSearchResults, NavigatorSearchResults);
        }

        private void PerkAllowances(HSession session, HMessage message)
        {
            if (session.ControllerId != "scout") return;

            Task.Factory.StartNew(() =>
            {
                while (true)
                {
                    motel.RoomQueue = new Queue<HabboRoom>();

                    foreach (var category in motel.RoomCategories)
                    {
                        session.Remote.SendPacketAsync(motel.OutHeaders.NavigatorSearchRequest, category, "").Wait();
                        Thread.Sleep(500);
                    }

                    while (motel.RoomQueue.Count > 0)
                        Thread.Sleep(1000);
                }
            }, TaskCreationOptions.LongRunning);
        }

        private void NavigatorSearchResults(HSession session, HMessage packet)
        {
            packet.SkipString();
            packet.SkipString();

            int count = packet.ReadInteger();

            for (int i = 0; i < count; i++)
            {
                string categoryName = packet.ReadString();
                string categoryText = packet.ReadString();
                packet.SkipInteger();
                packet.SkipBoolean();
                packet.SkipInteger();

                int count2 = packet.ReadInteger();
                for (int i2 = 0; i2 < count2; i2++)
                {
                    int flatId = packet.ReadInteger();
                    string roomName = packet.ReadString();
                    packet.SkipInteger();
                    string ownerName = packet.ReadString();
                    packet.SkipInteger();
                    int userCount = packet.ReadInteger();
                    int maxUsers = packet.ReadInteger();
                    string description = packet.ReadString();
                    packet.SkipInteger();
                    int score = packet.ReadInteger();
                    int ranking = packet.ReadInteger();
                    packet.SkipInteger();

                    int count3 = packet.ReadInteger();
                    for (int i3 = 0; i3 < count3; i3++)
                        packet.SkipString();

                    int count4 = packet.ReadInteger();
                    if ((count4 & 1) > 0)
                        packet.SkipString();

                    if ((count4 & 2) > 0)
                    {
                        packet.SkipInteger();
                        packet.SkipString();
                        packet.SkipString();
                    }

                    if ((count4 & 4) > 0)
                    {
                        packet.SkipString();
                        packet.SkipString();
                        packet.SkipInteger();
                    }

                    if (userCount > 0)
                        if (!motel.Rooms.ContainsKey(flatId))
                            motel.RoomQueue.Enqueue(new HabboRoom(categoryName, categoryText, flatId, ownerName, roomName, description, userCount, maxUsers, score, ranking));
                }
            }

            Program.Log($"[scout] Gathered {motel.RoomQueue.Count} rooms!");
        }
    }
}