﻿using ElmerFudd.Server.Habbo;
using ElmerFudd.Server.Network.Protocol;

namespace ElmerFudd.Server.Services
{
    public class InitService : ElmerService
    {
        private Motel motel;
        public InitService(Motel _motel)
        {
            motel = _motel;

            Commands.Add(motel.InHeaders.CfhTopicsInit, CfhTopicsInit);
            Commands.Add(motel.InHeaders.HotelViewInit, HotelViewInit);
            Commands.Add(motel.InHeaders.HotelPromoResponse, HotelPromoResponse);
        }

        private void CfhTopicsInit(HSession session, HMessage message)
        {
            session.Remote.SendPacketAsync(motel.OutHeaders.UserDataRequest).Wait();
            session.Remote.SendPacketAsync(motel.OutHeaders.HotelViewRequest, "").Wait();
        }

        private void HotelViewInit(HSession session, HMessage message)
        {
            session.Remote.SendPacketAsync(motel.OutHeaders.HotelPromoRequest).Wait();
        }

        private void HotelPromoResponse(HSession session, HMessage message)
        {
            session.Remote.SendPacketAsync(motel.OutHeaders.HotelViewRequest, "").Wait();
            session.Remote.SendPacketAsync(motel.OutHeaders.Unknown0).Wait();
            session.Remote.SendPacketAsync(motel.OutHeaders.LoginOK, "Login", "socket", "client.auth_ok", "").Wait();
        }
    }
}
