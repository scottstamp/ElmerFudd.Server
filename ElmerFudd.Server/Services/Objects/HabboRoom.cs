﻿namespace ElmerFudd.Server.Services.Objects
{
    public class HabboRoom
    {
        public string CategoryName { get; private set; }
        public string CategoryText { get; private set; }
        public int FlatId { get; private set; }
        public string OwnerName { get; private set; }
        public string RoomName { get; private set; }
        public string Description { get; private set; }
        public int UsersCount { get; private set; }
        public int MaxUsers { get; private set; }
        public int Score { get; private set; }
        public int Ranking { get; private set; }

        public HabboRoom(string categoryName, string categoryText, int flatId,
            string ownerName, string roomName, string description, int usersCount,
            int maxUsers, int score, int ranking)
        {
            CategoryName = categoryName;
            CategoryText = categoryText;
            FlatId = flatId;
            OwnerName = ownerName;
            RoomName = roomName;
            Description = description;
            UsersCount = usersCount;
            MaxUsers = maxUsers;
            Score = score;
            Ranking = ranking;
        }
    }
}
