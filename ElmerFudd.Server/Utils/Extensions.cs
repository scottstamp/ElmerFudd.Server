﻿using System;
using ElmerFudd.Server.Habbo;

namespace ElmerFudd.Server.Utils
{
    public static class Extensions
    {
        public static string ToDomain(this HHotel hotel)
        {
            string value = hotel.ToString().ToLower();
            return value.Length != 5 ? value : value.Insert(3, ".");
        }

        /// <summary>
        /// Returns a new string that begins where the parent ends in the source.
        /// </summary>
        /// <param name="source">The string that contains the child.</param>
        /// <param name="parent">The string that comes before the child.</param>
        /// <returns></returns>
        public static string GetChild(this string source, string parent)
        {
            if (string.IsNullOrWhiteSpace(source))
                return string.Empty;

            int sourceIndex = source
                .IndexOf(parent ?? string.Empty, StringComparison.OrdinalIgnoreCase);

            return sourceIndex >= 0 ?
                source.Substring(sourceIndex + parent.Length) : string.Empty;
        }
        /// <summary>
        /// Returns a new string that ends where the child begins in the source.
        /// </summary>
        /// <param name="source">The string that contains the parent.</param>
        /// <param name="child">The string that comes after the parent.</param>
        /// <returns></returns>
        public static string GetParent(this string source, string child)
        {
            if (string.IsNullOrWhiteSpace(source))
                return string.Empty;

            int sourceIndex = source
                .IndexOf(child ?? string.Empty, StringComparison.OrdinalIgnoreCase);

            return sourceIndex >= 0 ?
                source.Remove(sourceIndex) : string.Empty;
        }
        /// <summary>
        /// Returns a new string that is between the delimiters, and the child in the source.
        /// </summary>
        /// <param name="source">The string that contains the parent.</param>
        /// <param name="child">The string that comes after the parent.</param>
        /// <param name="delimiters">The Unicode characters that will be used to split the parent, returning the last split value.</param>
        /// <returns></returns>
        public static string GetParent(this string source, string child, params char[] delimiters)
        {
            string parentSource = source.GetParent(child);
            if (!string.IsNullOrWhiteSpace(parentSource))
            {
                string[] childSplits = parentSource.Split(delimiters,
                    StringSplitOptions.RemoveEmptyEntries);

                return childSplits[childSplits.Length - 1];
            }
            else return string.Empty;
        }
        /// <summary>
        /// Returns a new string that is between the parent, and the delimiters in the source.
        /// </summary>
        /// <param name="source">The string that contains the child.</param>
        /// <param name="parent">The string that comes before the child.</param>
        /// <param name="delimiters">The Unicode characters that will be used to split the child, returning the first split value.</param>
        /// <returns></returns>
        public static string GetChild(this string source, string parent, params char[] delimiters)
        {
            string childSource = source.GetChild(parent);

            return !string.IsNullOrWhiteSpace(childSource) ?
                childSource.Split(delimiters, StringSplitOptions.RemoveEmptyEntries)[0] : string.Empty;
        }
    }
}