﻿using System;
using System.IO;
using System.Net;
using System.Linq;
using System.Net.Http;
using System.Reflection;
using System.Net.Sockets;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;

using ElmerFudd.Server.Utils;
using ElmerFudd.Server.Habbo;
using ElmerFudd.Server.Network;
using ElmerFudd.Server.Services;
using ElmerFudd.Server.Network.Protocol;

using Flazzy;
using System.Collections;

namespace ElmerFudd.Server
{
    public class Program
    {
        private readonly object _logLock;
        private readonly TcpListener _listener;

        private static readonly HttpClient _client;
        private static readonly HttpClientHandler _clientHandler;

        private const int LISTEN_PORT = 8765;
        private const string EXTERNAL_VARIABLES_FORMAT = "https://www.habbo.{0}/gamedata/external_variables";
        private const string CHROME_USER_AGENT = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36";

        public DirectoryInfo ClientsDirectory { get; }
        public Dictionary<HHotel, Motel> Motels { get; }

        public Dictionary<string, HNode> Controllers { get; }
        public ObservableCollection<HSession> Sessions { get; }

        public Dictionary<ushort, Action<HNode, HMessage>> Commands { get; }

        static Program()
        {
            _clientHandler = new HttpClientHandler();
            _client = new HttpClient(_clientHandler);
            _client.DefaultRequestHeaders.Add("User-Agent", CHROME_USER_AGENT);
        }
        public Program(string[] args)
        {
            _logLock = new object();
            _listener = new TcpListener(IPAddress.Any, LISTEN_PORT);

            Motels = new Dictionary<HHotel, Motel>();
            Controllers = new Dictionary<string, HNode>();

            string clientsPath = Path.Combine(Path.GetDirectoryName(Assembly.GetEntryAssembly().Location), "Clients");
            ClientsDirectory = Directory.CreateDirectory(clientsPath);

            Sessions = new ObservableCollection<HSession>();
            Sessions.CollectionChanged += Sessions_CollectionChanged;

            Commands = new Dictionary<ushort, Action<HNode, HMessage>>
            {
                [1] = LoadSession,
                [2] = BranchData,
                [3] = SetNavigatorCategories,
                [4] = ClearAllSessions,
                [5] = UpdatePhrases,
				[6] = UpdateRaidRoomId,
				[7] = UpdateRunToRandomTile,
				[8] = UpdateChangeLooksOnEnter,
				[9] = UpdateShouldShout,
				[10] = UpdateShouldLeave
            };
        }
        public static void Main(string[] args)
        {
            var app = new Program(args);
            try
            {
                //Console.Title = "ElmerFudd.Server";
                Console.CursorVisible = false;

                app.RunAsync().Wait();
            }
            finally { Console.CursorVisible = true; }
        }

        private async Task RunAsync()
        {
            Log("Loading Motels...");
            await LoadHotelsAsync().ConfigureAwait(false);

            _listener.Start();
            UpdateTitle();

            Log("Listener Started");
            while (true)
            {
                try
                {
                    Socket client = await _listener.AcceptSocketAsync().ConfigureAwait(false);
                    Log("Controller Accepted: " + client.RemoteEndPoint);

                    var controller = new HNode(client);
                    Task handleControllerTask = HandleControllerAsync(controller);
                }
                catch (Exception ex) { Log(ex); }
            }
        }
        private async Task HandleSessionAsync(HSession session)
        {
            await Task.Yield();
            using (session)
            {
                await session.ConnectAsync().ConfigureAwait(false);
                if (session.Remote.IsConnected)
                {
                    Log($"[{session.ControllerId}] Session Connected: {session.Ticket}");
                }
                while (session.Remote.IsConnected)
                {
                    HMessage packet = await session.Remote.ReceivePacketAsync().ConfigureAwait(false);
                    //Log($"[{session.ControllerId}] Incoming: {packet.ToString()}");

                    if (packet == null) break;

                    foreach (ElmerService service in session.Motel.Services)
                    {
                        service.RunCommand(session, packet);
                    }
                }
            }
            Sessions.Remove(session);
        }
        private async Task HandleControllerAsync(HNode controller)
        {
            await Task.Yield();
            while (controller.IsConnected)
            {
                HMessage packet = await controller.ReceivePacketAsync().ConfigureAwait(false);
                if (packet == null) break;

                if (!Controllers.ContainsValue(controller))
                {
                    if (packet.Header == 0)
                    {
                        // TODO: Verification via login?
                        string id = packet.ReadString();
                        Controllers[id] = controller;

                        Log("Controller Verified: " + id);
                    }
                    else
                    {
                        controller.Disconnect();
                        Log($"Verification Failed: {controller.Client.RemoteEndPoint} -> {packet.Header}");
                    }
                }
                else if (Commands.TryGetValue(packet.Header, out Action<HNode, HMessage> command))
                {
                    command(controller, packet);
                    Log($"Command: {controller.Client.RemoteEndPoint} -> {packet.Header}");
                }
                else Log($"Invalid Command: {controller.Client.RemoteEndPoint} -> {packet.Header}");
            }
        }

        private async Task LoadHotelsAsync()
        {
            Dictionary<string, HGame> clients = LoadClients();
            var hotels = (HHotel[])Enum.GetValues(typeof(HHotel));
            foreach (HHotel hotel in hotels)
            {
                if (hotel == HHotel.Unknown) continue;
                var variablesUrl = string.Format(EXTERNAL_VARIABLES_FORMAT, hotel.ToDomain());

                string revision = null;
                string flashClientUrl = null;
                using (var gameDataStream = new StreamReader(await _client.GetStreamAsync(variablesUrl).ConfigureAwait(false)))
                {
                    while (!gameDataStream.EndOfStream)
                    {
                        string line = await gameDataStream.ReadLineAsync().ConfigureAwait(false);
                        if (line.StartsWith("flash.client.url="))
                        {
                            int urlStart = (line.IndexOf('=') + 1);
                            flashClientUrl = ("http:" + line.Substring(urlStart) + "Habbo.swf");

                            int revisionStart = (line.IndexOf("gordon/") + 7);
                            revision = line.Substring(revisionStart, (line.Length - revisionStart) - 1);
                            break;
                        }
                    }
                }

                if (!clients.TryGetValue(revision, out HGame game))
                {
                    game = new HGame(await _client.GetStreamAsync(flashClientUrl).ConfigureAwait(false));
                    game.Disassemble();

                    game.GenerateMessageHashes();
                    clients.Add(revision, game);

                    string localPath = Path.Combine(ClientsDirectory.FullName, $"{revision}.swf");
                    File.WriteAllBytes(localPath, game.ToArray(CompressionKind.None));
                }
                Motels.Add(hotel, new Motel(hotel, game));
            }
        }
        private Dictionary<string, HGame> LoadClients()
        {
            var clientBuilds = new Dictionary<string, HGame>();
            foreach (FileInfo clientInfo in ClientsDirectory.GetFiles("*.swf"))
            {
                HGame game = null;
                bool isDisposing = false;
                try
                {
                    game = new HGame(clientInfo.FullName);
                    game.Disassemble();

                    if (!clientBuilds.ContainsKey(game.Revision))
                    {
                        game.GenerateMessageHashes();
                        clientBuilds[game.Revision] = game;
                    }
                    else isDisposing = true;
                }
                catch { isDisposing = true; }
                finally
                {
                    if (isDisposing)
                    {
                        game?.Dispose();
                    }
                }
            }
            return clientBuilds;
        }

        private string GetControllerId(HNode controller)
        {
            return Controllers.Single(c => c.Value == controller).Key;
        }
        private void Sessions_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            IList items = (e.Action == NotifyCollectionChangedAction.Add ? e.NewItems : e.OldItems);
            foreach (HSession session in items)
            {
                switch (e.Action)
                {
                    case NotifyCollectionChangedAction.Add:
                    {
                        Task handleSessionTask = HandleSessionAsync(session);
                        break;
                    }
                    case NotifyCollectionChangedAction.Remove:
                    {
                        Log($"[{session.ControllerId}] Session Disconnected: " + session.Ticket);
                        session.Remote.Disconnect();
                        break;
                    }
                }
            }
            UpdateTitle();
        }

        private void UpdatePhrases(HNode controller, HMessage packet)
        {
            var phrases = packet.ReadString();
            Log($"[global] Phrases updated: {String.Join(";", phrases.Split((char)13))}");

            foreach (var motel in Motels.Values)
                motel.Phrases = phrases.Split((char)13);
        }
        private void ClearAllSessions(HNode controller, HMessage packet)
        {
            foreach (var session in Sessions)
            {
                session.Remote.Disconnect();
                Sessions.Remove(session);
            }

            foreach (var cntrl in Controllers.Values)
                cntrl.Dispose();

            Controllers.Clear();

            Log("[global] All sessions and controllers cleared.");
        }
        private void SetNavigatorCategories(HNode controller, HMessage packet)
        {
            var cats = packet.ReadString();
            Log($"[global] Navigator categories updated: {String.Join(";", cats.Split((char)13))}");

            foreach (var motel in Motels.Values)
                motel.RoomCategories = cats.Split((char)13);
        }
        private void BranchData(HNode controller, HMessage packet)
        {
            HSession[] sessions = Sessions.ToArray();
            string controllerId = GetControllerId(controller);
            foreach (HSession session in sessions)
            {
                if (session.ControllerId == controllerId)
                {
                    int dataLength = packet.ReadInteger();
                    byte[] data = packet.ReadBytes(dataLength);
                    session.Remote.SendAsync(data);
                }
            }
        }
        private void LoadSession(HNode controller, HMessage packet)
        {
            var cookies = new CookieContainer();
            string host = packet.ReadString();
            int cookieCount = packet.ReadInteger();
            for (int i = 0; i < cookieCount; i++)
            {
                var cookieName = packet.ReadString();
                var cookieValue = packet.ReadString();

                if (cookieName.StartsWith("YPF")) cookieValue = "";

                cookies.Add(new Uri("https://www.habbo.com"), new Cookie(cookieName, cookieValue, "/", "www.habbo.com"));
            }

            HHotel hotel = HotelEndPoint.GetHotel(host);
            if (Motels.TryGetValue(hotel, out Motel motel))
            {
                var session = new HSession(motel, GetControllerId(controller), cookies);
                Sessions.Add(session);
            }
        }
		
		
		private void UpdateRaidRoomId(HNode controller, HMessage packet)
        {
            int newId = packet.ReadInteger();
			
            foreach (var motel in Motels.Values)
                motel.RaidRoomId = newId;
        }
		
		private void UpdateRunToRandomTile(HNode controller, HMessage packet)
        {
            bool newVal = (packet.ReadInteger() > 0);
			int min = packet.ReadInteger();
			int max = packet.ReadInteger();
			
            foreach (var motel in Motels.Values)
			{
				motel.RunToRandomTile = newVal;
				motel.RunToRandomTileMin = min;
				motel.RunToRandomTileMax = max;
			}
        }
		
		private void UpdateChangeLooksOnEnter(HNode controller, HMessage packet)
        {
            bool newVal = (packet.ReadInteger() > 0);
			
            foreach (var motel in Motels.Values)
                motel.ChangeLooksOnEnter = newVal;
        }
		
		private void UpdateShouldShout(HNode controller, HMessage packet)
        {
            bool newVal = (packet.ReadInteger() > 0);
			
            foreach (var motel in Motels.Values)
                motel.ShouldShout = newVal;
        }
		
		private void UpdateShouldLeave(HNode controller, HMessage packet)
        {
            bool newVal = (packet.ReadInteger() > 0);
			
            foreach (var motel in Motels.Values)
                motel.ShouldLeave = newVal;
        }
		
        private void UpdateTitle()
        {
            //Console.Title = $"ElmerFudd.Server[{LISTEN_PORT}]";
            //Console.Title += (" | Sessions: " + Sessions.Count);
        }

        public static void Log(string message)
        {
            //lock (_logLock)
            {
                ConsoleColor color = Console.ForegroundColor;

                Console.ForegroundColor = ConsoleColor.Green;
                Console.Write($"[{DateTime.Now}] ");

                Console.ForegroundColor = color;
                Console.WriteLine(message);
            }
        }
        public static void Log(Exception exception, string message = null)
        {
            //lock (_logLock)
            {
                ConsoleColor color = Console.ForegroundColor;

                Console.ForegroundColor = ConsoleColor.Green;
                Console.Write($"[{DateTime.Now}] ");

                Console.ForegroundColor = ConsoleColor.Red;
                Console.Write("[Unhandled Exception] ");

                Console.ForegroundColor = color;
                Console.WriteLine(message);
                Console.WriteLine(exception);
            }
        }
    }
}