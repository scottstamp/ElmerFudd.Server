﻿using System.Collections.Generic;

using ElmerFudd.Server.Network;
using ElmerFudd.Server.Services;
using ElmerFudd.Server.Habbo.Headers;
using ElmerFudd.Server.Services.Objects;
using System;

namespace ElmerFudd.Server.Habbo
{
    public class Motel
    {
        public Incoming InHeaders { get; }
        public Outgoing OutHeaders { get; }

        public HGame Game { get; }
        public HotelEndPoint EndPoint { get; }
        public List<ElmerService> Services { get; }

        public Dictionary<int, HabboRoom> Rooms = new Dictionary<int, HabboRoom>();
        public Queue<HabboRoom> RoomQueue = new Queue<HabboRoom>();

        public Random Randomizer = new Random();
        public string[] Phrases = new string[0];
		
		
		public int RaidRoomId = -1;
		public bool RunToRandomTile = false;
		public int RunToRandomTileMin = 0;
		public int RunToRandomTileMax = 20;
		public bool ChangeLooksOnEnter = true;
		public bool ShouldShout = true;
		public bool ShouldLeave = true;
		

        public string[] Figures = new string[] {
            "ch-225-64.hr-893-45.lg-280-82.sh-290-1408.ha-1002-1408.hd-180-10",
            "ch-3030-90.hr-3090-45.lg-3088-90-91.ha-1007-0.fa-1202-73.hd-209-1",
            "ch-875-1408-1408.hr-170-45.hd-209-8.lg-280-1408",
            "ch-220-64.hr-155-45.lg-270-64.sh-300-64.ha-1013-64.wa-2001-0.fa-1201-0.hd-209-8.cc-260-64",
            "ch-210-71.hr-125-39.hd-190-1370.lg-3116-1408-1408",
            "ch-3109-1408-64.hr-155-45.hd-209-1370.lg-3088-64-1408.sh-300-64",
            "ch-255-1408.hr-891-40.lg-3116-76-1408.sh-906-1408.ha-1003-64.hd-190-1.cp-3286-0",
            "ch-808-82.hr-3090-37.lg-275-82.sh-295-73.ha-3117-73.fa-1201-0.hd-180-1",
            "ch-230-66.hr-165-42.lg-270-73.sh-295-73.ha-1002-64.ca-1804-73.hd-209-1370",
            "ch-210-64.fa-1205-73.hd-180-10.lg-3023-84.ha-1014-0",
            "he-1608-0.ch-255-64.hr-170-45.lg-280-64.sh-305-1408.fa-3276-72.hd-205-1370"
        };

        public string[] RoomCategories = new string[] {
            "popular",
            "hotel_view",
            "recommended",
            "category__Global Chat & Discussion",
            "category__Trading",
            "category__global games",
            "category__Agencies",
            "category__Role Playing",
            "category__GLOBAL BUILDING AND DECORATION",
            "category__global party",
            "category__global help"
        };

        public Motel(HHotel hotel, HGame game)
        {
            Game = game;
            EndPoint = HotelEndPoint.Parse(hotel);

            InHeaders = Incoming.Create(game);
            OutHeaders = Outgoing.Create(game);

            Services = new List<ElmerService>
            {
                new HeartbeatService(this),
                new InitService(this),
                new HubboNavService(this),
                new HubboSpamService(this)
            };
        }
    }
}