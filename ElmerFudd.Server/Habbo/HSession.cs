﻿using System;
using System.Threading.Tasks;

using ElmerFudd.Server.Utils;
using ElmerFudd.Server.Crypto;
using ElmerFudd.Server.Network;
using ElmerFudd.Server.Network.Protocol;
using System.Net;
using System.IO;

namespace ElmerFudd.Server.Habbo
{
    public class HSession : IDisposable
    {
        public Motel Motel { get; }
        public HNode Remote { get; private set; }
        public string ControllerId { get; }

        public string Ticket { get; set; }
        public CookieContainer Cookies { get; set; }

        public bool IsMuted = false;
        public bool CookiesWork = true;

        public HSession(Motel motel, string controllerId, CookieContainer cookies)
        {
            Motel = motel;
            Remote = new HNode();
            ControllerId = controllerId;
            Cookies = cookies;
        }

        private string GetSsoToken(CookieContainer cookies)
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create("https://www.habbo.com/api/ssotoken");
            request.Headers[HttpRequestHeader.UserAgent] = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36";
            request.CookieContainer = cookies;

            WebResponse response = request.GetResponseAsync().Result;

            string sso = new StreamReader(response.GetResponseStream()).ReadToEnd();
            //Console.WriteLine(sso);

            if (sso.Contains("ssoToken"))
            {
                PersistCookies(cookies);
                return sso.Split('"')[3];
            }
            else return "";
        }

        private void PersistCookies(CookieContainer cookies)
        {
            Task.Factory.StartNew(() =>
            {
                while (true)
                {
                    HttpWebRequest request = (HttpWebRequest)WebRequest.Create("https://www.habbo.com/api/user/ping");
                    request.Headers[HttpRequestHeader.UserAgent] = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36";
                    request.CookieContainer = cookies;

                    string response = new StreamReader(request.GetResponseAsync().Result.GetResponseStream()).ReadToEnd();

                    Program.Log($"[{ControllerId}] Refreshed Cookies: {response}");

                    Task.Delay(600000).Wait();
                }
            });
        }

        public async Task ConnectAsync()
        {
            Remote = new HNode();

            Ticket = GetSsoToken(Cookies);

            if (Ticket == "")
            {
                CookiesWork = false;
                return;
            }

            var timeStamp = DateTime.Now;
            await Remote.ConnectAsync(Motel.EndPoint).ConfigureAwait(false);

            await Remote.SendPacketAsync(Motel.OutHeaders.Hello, Motel.Game.Revision, "FLASH", 1, 0).ConfigureAwait(false);
            await Remote.SendPacketAsync(Motel.OutHeaders.InitCrypto).ConfigureAwait(false);

            HMessage pgPacket = await Remote.ReceivePacketAsync().ConfigureAwait(false);
            var exchange = new HKeyExchange(65537, "e052808c1abef69a1a62c396396b85955e2ff522f5157639fa6a19a98b54e0e4d6e44f44c4c0390fee8ccf642a22b6d46d7228b10e34ae6fffb61a35c11333780af6dd1aaafa7388fa6c65b51e8225c6b57cf5fbac30856e896229512e1f9af034895937b2cb6637eb6edf768c10189df30c10d8a3ec20488a198063599ca6ad");

            string p = pgPacket.ReadString();
            string g = pgPacket.ReadString();
            exchange.VerifyDHPrimes(p, g);

            exchange.Padding = PKCSPadding.RandomByte;
            await Remote.SendPacketAsync(Motel.OutHeaders.DHPublicKey, exchange.GetPublicKey()).ConfigureAwait(false);
            HMessage serverKeyPacket = await Remote.ReceivePacketAsync().ConfigureAwait(false);

            byte[] sharedKey = exchange.GetSharedKey(serverKeyPacket.ReadString());
            Remote.Encrypter = new RC4(sharedKey);
            Remote.IsEncrypting = true;

            string flashClientUrl = $"https://images.habbo.com/gordon/{Motel.Game.Revision}/";
            string externalVariablesUrl = $"https://www.habbo.{Motel.EndPoint.Hotel.ToDomain()}/gamedata/external_variables/1";

            await Remote.SendPacketAsync(Motel.OutHeaders.GameVariables, 401, flashClientUrl, externalVariablesUrl).ConfigureAwait(false);
            await Remote.SendPacketAsync(Motel.OutHeaders.SsoTicket, Ticket, (DateTime.Now - timeStamp).Seconds).ConfigureAwait(false);
        }

        public void Dispose()
        {
            Dispose(true);
        }
        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                Remote?.Dispose();
            }
        }
    }
}
