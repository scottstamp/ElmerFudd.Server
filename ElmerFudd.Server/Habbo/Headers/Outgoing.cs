﻿using System.Linq;
using System.Reflection;
using ElmerFudd.Server.Network.Protocol;

namespace ElmerFudd.Server.Habbo.Headers
{
    public class Outgoing
    {
        public static readonly PropertyInfo[] _outProperties;

        static Outgoing()
        {
            _outProperties = typeof(Outgoing).GetProperties()
                .Where(p => p.CustomAttributes.Any())
                .ToArray();
        }

        public ushort Hello { get; } = 4000;

        [MessageHash("5eb8e11fcf3095cb09df076aeafd80c8")]
        public ushort InitCrypto { get; private set; }

        [MessageHash("756d959a0370648144257bf9b780421f")]
        public ushort DHPublicKey { get; private set; }

        [MessageHash("4ff1277489a39f5e68ce732c81ce453c")]
        public ushort GameVariables { get; private set; }

        [MessageHash("7e17678ff38a73753cc90d1673a45696")]
        public ushort SsoTicket { get; private set; }

        [MessageHash("7dccddd7297f447ced9659989feba3b5")]
        public ushort Pong { get; private set; }

        [MessageHash("27c2015ae485a6eadb6b631159889706")]
        public ushort UserDataRequest { get; private set; }

        [MessageHash("f589f9b57ef74fc96a35ad2f1cd4392a")]
        public ushort HotelViewRequest { get; private set; }

        [MessageHash("15c25e31ba6b5a4ccf51c8dd89833b88")]
        public ushort HotelPromoRequest { get; private set; }

        [MessageHash("15c25e31ba6b5a4ccf51c8dd89833b88")]
        public ushort Unknown0DA { get; private set; }

        [MessageHash("e6031d387e5b031c4169ab47580c2ed3")]
        public ushort RoomNavigate { get; private set; }

        [MessageHash("8e6f1e32c63528eb23ef78ae35c34fff")]
        public ushort Walk { get; private set; }

        [MessageHash("139f0daa95f5033f3feec516a2d1779a")]
        public ushort ToggleObject { get; private set; }

        [MessageHash("1a21657e4661d902096ce7af11a9f859")]
        public ushort Unknown0 { get; private set; }

        [MessageHash("9b39bd15987e64881c140adecfffb303")]
        public ushort LoginOK { get; set; }

        [MessageHash("f209ff344261986a9b4a4ddd84a50422")]
        public ushort Shout { get; internal set; }

        [MessageHash("46d152f9ed5fe274871d8ac48bdd4a7f")]
        public ushort RoomNavSequence1 { get; internal set; }

        [MessageHash("0dde741229b1bd7076592e1aea69c208")]
        public ushort RoomNavSequence2 { get; internal set; }

        [MessageHash("eaee84fbf06336b39cc2fe5b73897747")]
        public ushort RoomNavSequence3 { get; internal set; }

        [MessageHash("246f4a99fcb0ca6718eab84f83c6ab91")]
        public ushort NavigatorSearchRequest { get; internal set; }
        
        [MessageHash("11978d35fa885568bacf353829b49def")]
        public ushort RespectUser { get; internal set; }
		
		/*[MessageHash("cb9cd754ab5465b64d2c9e0d3d539aee")]
        public ushort ChangeLooks { get; internal set; }*/
		
		/*
		[cb9cd754ab5465b64d2c9e0d3d539aee]
		Outgoing[3607, _-4dW] -> [0][0][0]2[0][1]M[0]+hr-125-42.ch-3030-1408.hd-185-1.lg-280-1408
		{u:3607}{s:M}{s:hr-125-42.ch-3030-1408.hd-185-1.lg-280-1408}
		*/

        public static Outgoing Create(HGame game)
        {
            var outHeaders = new Outgoing();
            foreach (PropertyInfo outProperty in _outProperties)
            {
                string hash = outProperty.GetCustomAttribute<MessageHashAttribute>().Hash;
                ushort id = game.Messages[hash].Single().Id;

                outProperty.SetValue(outHeaders, id);
            }
            return outHeaders;
        }
    }
}