﻿using System;

namespace ElmerFudd.Server.Habbo.Headers
{
    [AttributeUsage(AttributeTargets.Property)]
    public class MessageHashAttribute : Attribute
    {
        public string Hash { get; }

        public MessageHashAttribute(string hash)
        {
            Hash = hash;
        }
    }
}