﻿using System.Linq;
using System.Reflection;

namespace ElmerFudd.Server.Habbo.Headers
{
    public class Incoming
    {
        public static readonly PropertyInfo[] _inProperties;

        static Incoming()
        {
            _inProperties = typeof(Incoming).GetProperties()
                .Where(p => p.CustomAttributes.Any())
                .ToArray();
        }

        [MessageHash("90e3c1a9440fd34f94355eb07030e75d")]
        public ushort CfhTopicsInit { get; private set; }
        [MessageHash("1b97a586021e2454ba8e6adbe93b01e7")]
        public ushort HotelViewInit { get; private set; }
        [MessageHash("3082099f8f1f32773feab987003ce21b")]
        public ushort HotelPromoResponse { get; private set; }
        [MessageHash("aa0e08f6e6e2435a73a00ead6b928fbc")]
        public ushort PerkAllowances { get; private set; }
        [MessageHash("b0371faae80ebd6e64a93271215dabae")]
        public ushort Ping { get; private set; }
        [MessageHash("fbed6c350795d69397e642e4c964f259")]
        public ushort NavigatorSearchResults { get; internal set; }
        [MessageHash("e3795caf5cc13497ba96fe78eafca7d1")]
        public ushort RoomModelInfo { get; internal set; }
        [MessageHash("c925343408a31ece2208623fc2d9ff98")]
        public ushort HeightMapMessage { get; internal set; }
        [MessageHash("007d6331ffbf79be03a7b95eeffd42ad")]
        public ushort UserIsMuted { get; internal set; }

        public static Incoming Create(HGame game)
        {
            var inHeaders = new Incoming();
            foreach (PropertyInfo inProperty in _inProperties)
            {
                string hash = inProperty.GetCustomAttribute<MessageHashAttribute>().Hash;
                ushort id = game.Messages[hash].Single().Id;

                inProperty.SetValue(inHeaders, id);
            }
            return inHeaders;
        }
    }
}