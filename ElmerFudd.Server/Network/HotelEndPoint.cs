﻿using System;
using System.Net;

using ElmerFudd.Server.Utils;
using ElmerFudd.Server.Habbo;

namespace ElmerFudd.Server.Network
{
    public class HotelEndPoint : IPEndPoint
    {
        private string _host;
        public string Host
        {
            get { return _host; }
            set
            {
                _host = value;
                Hotel = GetHotel(value);
            }
        }

        public HHotel Hotel { get; private set; }

        public HotelEndPoint(IPEndPoint endPoint)
            : base(endPoint.Address, endPoint.Port)
        { }
        public HotelEndPoint(long address, int port)
            : base(address, port)
        { }
        public HotelEndPoint(IPAddress address, int port)
            : base(address, port)
        { }
        public HotelEndPoint(IPAddress address, int port, string host)
            : base(address, port)
        {
            Host = host;
        }

        public static HHotel GetHotel(string host)
        {
            if (string.IsNullOrWhiteSpace(host))
                return HHotel.Unknown;

            string identifier = host.GetChild(
                "game-", '.').Replace("us", "com");

            if (string.IsNullOrWhiteSpace(identifier))
            {
                if (host.StartsWith("."))
                    host = ("habbo" + host);

                identifier = host.GetChild("habbo.", '/')
                    .Replace(".", string.Empty);
            }
            else if (identifier == "tr" ||
                identifier == "br")
            {
                identifier = ("com" + identifier);
            }

            HHotel hotel;
            Enum.TryParse(identifier, true, out hotel);

            return hotel;
        }

        public static HotelEndPoint Parse(string host)
        {
            return Parse(GetHotel(host));
        }
        public static HotelEndPoint Parse(HHotel hotel)
        {
            int port = 0;
            var infix = string.Empty;
            switch (hotel)
            {
                case HHotel.Com:
                {
                    infix = "us";
                    port = 38101;
                    break;
                }
                default:
                {
                    port = 30000;
                    infix = hotel.ToString().ToLower();
                    if (infix.Length > 2)
                    {
                        infix = infix.Substring(3);
                    }
                    break;
                }
            }
            return Parse($"game-{infix}.habbo.com", port);
        }

        public static HotelEndPoint Parse(string host, int port)
        {
            IPAddress[] ips = Dns.GetHostAddressesAsync(host).Result;
            return new HotelEndPoint(ips[0], port, host);
        }
        public static bool TryParse(string host, int port, out HotelEndPoint endPoint)
        {
            try
            {
                endPoint = Parse(host, port);
                return true;
            }
            catch
            {
                endPoint = null;
                return false;
            }
        }

        public override string ToString()
        {
            if (!string.IsNullOrWhiteSpace(Host))
            {
                return (Host + ":" + Port);
            }
            return base.ToString();
        }
    }
}